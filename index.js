function Pokemon(name, level) {
    this.name = name;
    this.lvl = level;
    this.health = 2 * level;
    this.atk = level;

    //Methods
    this.faint = function(targetName) {
        console.log(`${targetName} fainted`);
    };

    this.tackle = function(target) {
        let result = target.health - this.atk;
        target.health = result;

        console.log(`${this.name} tackled ${target.name}`);

        if (target.health < 10) {
            this.faint(target.name);
        }

        console.log(`${target.name}'s health is now reduced to ${result}`);
    };
}

let bulbasaur = new Pokemon('bulbasaur', 16);
let rattata = new Pokemon('rattata', 8);